const mongoose = require('mongoose');
const Joi = require('joi');

const userJoiSchema = Joi.object({
  password: Joi.string()
    .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'ua'] } }),
});

const User = mongoose.model('users', {
  role: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  created_date: {
    type: Object,
  },
  password: {
    type: String,
    required: true,
  },
});

module.exports = {
  User,
  userJoiSchema,
};
