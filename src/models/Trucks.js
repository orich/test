const mongoose = require('mongoose');

const trucksSchema = mongoose.Schema({
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assignedTo: {
    type: String,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Object,
  },
});

const Trucks = mongoose.model('trucks', trucksSchema);

module.exports = {
  Trucks,
};
