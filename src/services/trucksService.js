const { Trucks } = require('../models/Trucks');
const { User } = require('../models/Users');
// const { trucksRouter } = require('../routers/trucksRouter.js');

async function addUserTrucks(req, res, next) {
  try {
    const { type } = req.body;
    const user = await User.findOne({ _id: req.user.userId, role: 'DRIVER' });
    console.log(req.user);
    if (type && user) {
      const truck = new Trucks({
        type,
        createdBy: req.user.userId,
        status: 'IS',
        createdDate: req._startTime,
        assignedTo: 'null',
      });
      truck.save().then(() => {
        res.json({
          message: 'Truck created successfully',
        });
      });
    } else {
      res.status(400).json({
        message: 'string',
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'string',
    });
  }
}

const getTruck = (req, res, next) => {
  try {
    if (req.url) {
      Trucks.findById(req.params.id)
        .then((truck) => {
          res.json({
            truck,
          });
        });
    } else {
      res.status(400).json({
        message: 'string',
      });
    }
  } catch (error) {
    res.status(400).json({
      message: 'string',
    });
  }
};

const deleteTruck = (req, res, next) => {
  try {
    if (req.url) {
      Trucks.findByIdAndDelete(req.params.id)
        .then(() => {
          res.json({
            message: 'Truck deleted successfully',
          });
        });
    } else {
      res.status(400).json({
        message: 'string',
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'string',
    });
  }
};

const getUserTrucks = (req, res, next) => {
  try {
    Trucks.find({ userId: req.user.userId }, '-__v')
      .then((result) => {
        res.json({
          trucks: result,
        });
      });
  } catch (error) {
    res.status(500).json({
      message: 'string',
    });
  }
};

const updateMyTruckById = (req, res, next) => {
  const { type } = req.body;
  return Trucks.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { type } })
    .then((result) => {
      res.json({ message: 'Truck details changed successfully' });
    });
};

const assignTruck = async (req, res, next) => {
  const truck = await Trucks.findById({ _id: req.params.id, userId: req.user.userId });

  if (truck) {
    Trucks.find({ userId: req.user.userId }, '-__v')
      .then((result) => {
        result.map((item) => {
          Trucks.findByIdAndUpdate(
            { _id: item._id },
            { $set: { assignedTo: 'null' } },
          ).then(() => console.log('hi'));
        });
      }).then(() => {
        Trucks.findByIdAndUpdate(
          { _id: req.params.id },
          { $set: { assignedTo: req.user.userId.toString() } },
        )
          .then(res.json({ message: 'Truck assigned successfully' }));
      });
  } else {
    res.status(400).json({ message: 'string' });
  }
};

// const markMyNoteCompletedById = (req, res, next) => Notes.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: true } })
//   .then((result) => {
//     res.json({ message: 'note was marked completed' });
//   });

module.exports = {
  addUserTrucks,
  getTruck,
  deleteTruck,
  getUserTrucks,
  updateMyTruckById,
  assignTruck,
};
