const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const { User, userJoiSchema } = require('../models/Users');

dotenv.config();

const createProfile = async (req, res, next) => {
  const { email, password, role } = req.body;

  await userJoiSchema.validateAsync({ email, password });

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
    created_date: req._startTime,
  });
  console.log(user);
  user.save()
    .then(() => res.json({ message: 'Success' }))
    .catch((error) => next(error));
};

const login = async (req, res, next) => {
  const { email, password } = req.body;

  await userJoiSchema.validateAsync({ email, password });

  const user = await User.findOne({ email: req.body.email });

  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, userId: user._id };
    const jwtToken = jwt.sign(payload, process.env.SECRET_KEY);
    return res.json({
      jwt_token: jwtToken,
    });
  }
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  await userJoiSchema.validateAsync({ email });

  const user = await User.findOne({ email: req.body.email });

  if (user) {
    res.json({ message: 'New password sent to your email address' });
  } else {
    res.status(400).json({ message: 'String' });
  }
};

module.exports = {
  createProfile,
  login,
  forgotPassword,
};
