const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/Users');

const getUserInfo = (req, res, next) => {
  try {
    User.findOne({ _id: req.user.userId })
      .then((result) => res.json({
        user: {
          _id: result._id,
          email: result.email,
          role: result.role,
          created_date: result.created_date,
        },
      }));
  } catch (error) {
    res.status(400).json({
      message: 'string',
    });
  }
};

const deleteProfile = (req, res, next) => {
  User.findOneAndDelete({ _id: req.user.userId })
    .then(() => res.json({
      message: 'Profile deleted successfully',
    }));
};

const changeProfilePassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  let userPassword;
  await User.findOne({ _id: req.user.userId })
    .then((result) => userPassword = result.password);
  if (await bcrypt.compare(String(oldPassword), String(userPassword))) {
    User.findOneAndUpdate({ _id: req.user.userId }, { $set: { password: await bcrypt.hash(newPassword, 10) } })
      .then((result) => {
        res.json({ message: 'password was updated' });
      });
  }
};

module.exports = {
  getUserInfo,
  deleteProfile,
  changeProfilePassword,
};
