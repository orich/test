const { Load } = require('../models/load.js');
const { User } = require('../models/Users');
// const { loadRouter } = require('../routers/loadRouter');

const addUserLoad = async (req, res, next) => {
  try {
    const user = await User.findOne({ _id: req.user.userId, role: 'SHIPPER' });
    const {
      name, payload, pickup_address, delivery_address, width, height, length,
    } = req.body;

    if (user) {
      const load = new Load({
        created_by: req.user.userId,
        assigned_to: 'null',
        status: 'NEW',
        state: 'En route to Pick Up',
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions: {
          width,
          height,
          length,
        },
        logs: [{
          message: 'Load assigned to driver with id ###',
          time: req._startTime,
        }],
        created_date: req._startTime,
      });

      load.save().then(() => {
        res.json({
          message: 'Load created successfully',
        });
      }).catch((error) => next(error));
    } else {
      res.status(400).json({ message: 'string' });
    }
  } catch (error) {
    res.status(400).json({ message: 'loh' });
  }
};

const getUserLoad = (req, res, next) => {
  try {
    Load.find({ userId: req.user.userId }, '-__v')
      .then((result) => {
        res.json({
          loads: result,
        });
      });
  } catch (error) {
    res.status(500).json({
      message: 'string',
    });
  }
};

const getLoad = (req, res, next) => {
  try {
    if (req.url) {
      Load.findById(req.params.id)
        .then((load) => {
          res.json({
            load,
          });
        });
    } else {
      res.status(400).json({
        message: 'string',
      });
    }
  } catch (error) {
    res.status(400).json({
      message: 'string',
    });
  }
};
//
const deleteLoad = (req, res, next) => {
  try {
    if (req.url) {
      Load.findByIdAndDelete(req.params.id)
        .then(() => {
          res.json({
            message: 'Load deleted successfully',
          });
        });
    } else {
      res.status(400).json({
        message: 'string',
      });
    }
  } catch (error) {
    res.status(500).json({
      message: 'string',
    });
  }
};
//
// const getUserTrucks = (req, res, next) => {
//     try {
//         Trucks.find({ userId: req.user.userId }, '-__v')
//             .then((result) => {
//                 res.json({
//                     trucks: result
//                 });
//             });
//     } catch (error) {
//         res.status(500).json({
//             "message": "string"
//         });
//     }
// };
//
// const updateMyTruckById = (req, res, next) => {
//     const { type } = req.body;
//     return Trucks.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { type } })
//         .then((result) => {
//             res.json({ message: 'Truck details changed successfully' });
//         });
// };
//
// const assignTruck = async (req, res, next) => {
//
//     const truck = await Trucks.findById({_id: req.params.id, userId: req.user.userId});
//
//     if (truck) {
//         Trucks.find({ userId: req.user.userId }, '-__v')
//             .then((result) => {
//                 result.map(item => {
//                     Trucks.findByIdAndUpdate({ _id: item._id },
//                         { $set: { assignedTo: 'null' } }).then(() => console.log('hi'))
//                 })}).then(() => {
//             Trucks.findByIdAndUpdate({ _id: req.params.id },
//                 { $set: { assignedTo: req.user.userId.toString() } })
//                 .then(res.json({message: 'Truck assigned successfully'
//                 }));
//         });
//     } else {
//         res.status(400).json({message: 'string'});
//     }
// }

// const markMyNoteCompletedById = (req, res, next) => Notes.findByIdAndUpdate({ _id: req.params.id, userId: req.user.userId }, { $set: { completed: true } })
//   .then((result) => {
//     res.json({ message: 'note was marked completed' });
//   });

module.exports = {
  addUserLoad,
  getUserLoad,
  getLoad,
  deleteLoad,
};
