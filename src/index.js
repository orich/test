const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

app.use(
  express.urlencoded({
    extended: true,
  }),
);

const { trucksRouter } = require('./routers/trucksRouter');
const { usersRouter } = require('./routers/usersRouter');
const { authRouter } = require('./routers/authRouter');
const { loadRouter } = require('./routers/loadRouter');

app.use(express.json());
app.use(morgan('tiny'));

// app.use('/api/trucks', trucksRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
// app.use('/api/loads', loadRouter);

mongoose.connect('mongodb+srv://root:root@oyclustertest.7pq00ym.mongodb.net/uber_db?retryWrites=true&w=majority');

async function start() {
  // await mongoose.connect('mongodb+srv://root:root@oyclustertest.7pq00ym.mongodb.net/uber_db?retryWrites=true&w=majority');
  app.listen(8080);
}

start();

app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
