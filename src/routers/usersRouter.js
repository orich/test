const express = require('express');

const router = express.Router();
const { getUserInfo, deleteProfile, changeProfilePassword } = require('../services/usersService.js');
const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getUserInfo);

router.delete('/me', authMiddleware, deleteProfile);

router.patch('/me/password', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};
