const express = require('express');

const router = express.Router();
const {
  addUserTrucks, getTruck, deleteTruck, getUserTrucks, updateMyTruckById,
  assignTruck,
} = require('../services/trucksService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addUserTrucks);

router.get('/', authMiddleware, getUserTrucks);

router.get('/:id', getTruck);

router.put('/:id', authMiddleware, updateMyTruckById);

router.post('/:id/assign', authMiddleware, assignTruck);

router.delete('/:id', deleteTruck);

module.exports = {
  trucksRouter: router,
};
