const express = require('express');

const router = express.Router();
const {
  addUserLoad, getUserLoad, getLoad, deleteLoad,
} = require('../services/loadService');
const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, addUserLoad);

router.get('/', authMiddleware, getUserLoad);

router.get('/:id', getLoad);
//
// router.put('/:id', authMiddleware, updateMyTruckById);
//
// router.post('/:id/assign', authMiddleware, assignTruck);
//
router.delete('/:id', deleteLoad);

module.exports = {
  loadRouter: router,
};
