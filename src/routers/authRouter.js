const express = require('express');

const router = express.Router();
const { createProfile, login, forgotPassword } = require('../services/authService.js');

const asyncWrapper = (controller) => (req, res, next) => controller(req, res, next).catch(next);

router.post('/register', asyncWrapper(createProfile));

router.post('/login', asyncWrapper(login));

router.post('/forgot_password', asyncWrapper(forgotPassword));

module.exports = {
  authRouter: router,
};
